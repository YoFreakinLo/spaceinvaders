class Projectile
{
    constructor(position, speed)
    {
        this._speed = speed;
        this._position = {x: position.x, y: position.y};
        this._radius = 5;
    }

    update()
    {
        this.position.y -= this.speed;
        if(this.position.y < this._radius)
        {
            global.remove(this, 'projectiles');
        }
    }

    draw(ctx)
    {
        ctx.beginPath();
        ctx.fillStyle = 'red';
        ctx.arc(this.position.x, this.position.y, 5, 0, Math.PI * 360);
        ctx.fill();
    }

    collidedWith(entity)
    {
        global.remove(entity, 'enemies');
    }

    get speed()
    {
        return this._speed;
    }

    get position()
    {
        return this._position;
    }

    get radius()
    {
        return this._radius;
    }
}

exports['Projectile'] = Projectile;