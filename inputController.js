class InputController
{
    static _player;
    static _keyMap = {};
    static _wasInit = false;
    static init()
    {
        if(InputController._wasInit)
        {
            return;
        }

        InputController._wasInit = true;

        window.onkeydown = (e) =>
        {
            InputController._keyMap[e.key] = true;
            e.preventDefault();
        };

        window.onkeyup = (e) => 
        {
            InputController._keyMap[e.key] = false;
            e.preventDefault();
        }
    }

    static isKeyPressed(key)
    {
        if(!InputController._wasInit)
        {
            throw new Error("InputController was not initialized");
        }
        if(InputController._keyMap[key])
        {
            return true;
        }
        return false;
    }
}

(()=>
{
    InputController.init();
})();

exports['InputController'] = InputController;