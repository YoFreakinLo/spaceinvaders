const {Player} = require('./player');
class Enemy
{
    constructor(position, speed)
    {
        this._position = position;
        this._speed = speed;
        this._radius = 30;
    }

    update()
    {
        this._position.y += this._speed;
        if(this.position.y - this.radius >= global.canvas.height)
        {
            global.player.damage();
            global.remove(this, enemies);
        }
    }

    draw(ctx)
    {
        ctx.beginPath();
        ctx.fillStyle = 'black';
        ctx.arc(this.position.x, this.position.y, this._radius, 0, Math.PI * 360);
        ctx.fill();
    }

    collidedWith(entity)
    {
        if(entity instanceof Player)
        {
            entity.damage();
        }
        else
        {
            global.remove(entity, 'projectiles');
        }
    }

    get position()
    {
        return {x: this._position.x, y: this._position.y};
    }

    set position(position)
    {
        if('x' in position && 'y' in position)
        {
            this._position.x = position.x;
            this._position.y = position.y;
        }
    }

    get radius()
    {
        return this._radius;
    }
}

exports['Enemy'] = Enemy;