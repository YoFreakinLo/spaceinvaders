const {InputController} = require('./inputController');
const {Projectile} = require('./projectile');

class Player
{
  constructor(position)
  {
    this._position = position;
    this._speed = 5;
    this._radius = 20;
    this._lastFired = 0;
    this._fireDelay = 200;
    this._lifes = 5;
  }
  
  update()
  {
    let vertical = InputController.isKeyPressed('w') ? -1 : 0;
    vertical += InputController.isKeyPressed('s') ? 1 : 0;
    let horizontal = InputController.isKeyPressed('a') ? -1 : 0;
    horizontal += InputController.isKeyPressed('d') ? 1 : 0;
    
    let newXPos = this.position.x + this.speed * horizontal;
    let newYPos = this.position.y + this.speed * vertical;
    this._position.x = newXPos;
    this._position.y = newYPos;
    
    if(newXPos - this.radius < 0)
    {
      this._position.x = this.radius;
    }
    if(newXPos + this.radius > global.canvas.width)
    {
      this._position.x = global.canvas.width - this.radius;
    }
    if(newYPos - this.radius < 0)
    {
      this._position.y = this.radius;
    }
    if(newYPos + this.radius > global.canvas.height)
    {
      this._position.y = global.canvas.height - this.radius;
    }
    
    if(InputController.isKeyPressed(' '))
    {
      const now = new Date().getTime();
      if(now - this._fireDelay >= this._lastFired)
      {
        this._lastFired = now;
        let projectile = new Projectile(this.position, 10);
        global.add(projectile, 'projectiles');
      }
    }
  }
  
  draw(ctx)
  {
    ctx.beginPath();
    ctx.fillStyle = 'red';
    ctx.arc(this.position.x, this.position.y, this._radius, 0, Math.PI * 360);
    ctx.fill();
  }

  collidedWith(entity)
  {
    global.remove(entity, 'enemies');
  }

  damage()
  {
    this._lifes--;
    console.log(this._lifes);
  }

  get position()
  {
    return {x: this._position.x, y: this._position.y};
  }
  
  set position(position)
  {
    if('x' in position && 'y' in position)
    {
      this._position.x = position.x;
      this._position.y = position.y;
    }
  }
  get speed()
  {
    return this._speed;
  }

  set speed(speed)
  {
    if(typeof speed === 'number')
    {
      this._speed = speed;
    }
  }

  get radius()
  {
    return this._radius;
  }

  get lifes()
  {
    return this._lifes;
  }
}
 
exports['Player'] = Player;
