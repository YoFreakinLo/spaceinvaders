// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

global.remove = function(entity, type)
{
  for(let i in global[type])
  {
    if(global[type][i] === entity)
    {
      global[type].splice(i, 1);
    }
  }
}

global.add = function(entity, type)
{
  if(typeof type === 'string')
  {
    global[type][global[type].length] = entity;
  }
}

const {Player} = require('./player');
const {Enemy} = require('./enemy');
const fs = require('fs');
const {app} = require('electron');

global.canvas = document.getElementById('display'),
  ctx = canvas.getContext('2d'),
  bW = parseInt(getComputedStyle(canvas, null).getPropertyValue('border-width'));

global.enemies = [];
global.projectiles = [];

//window.addEventListener('resize', resizeCanvas, false);
window.addEventListener('keydown', function (e) {
  if (e.key === 'F12') {
    require('remote').getCurrentWindow().toggleDevTools();
  } else if (e.which === 116) {
    location.reload();
  }
  else if(e.key === 'Escape')
  {
    app.quit();
  }
});

function checkCollision()
{
  for(let enemy of global.enemies)
  {
    let xOffset; 
    let yOffset;
    let distance;
    // Starting with the projectiles x enemies to destroy the enemy before it hits the player
    for(let projectile of global.projectiles)
    {
      xOffset = projectile.position.x < enemy.position.x ? enemy.position.x - projectile.position.x : projectile.position.x - enemy.position.x;
      yOffset = projectile.position.y < enemy.position.y ? enemy.position.y - projectile.position.y : projectile.position.y - enemy.position.y;
      distance = projectile.radius + enemy.radius;
      if(xOffset <= distance && yOffset <= distance)
      {
        enemy.collidedWith(projectile);
        projectile.collidedWith(enemy);
      }
    }
    xOffset = player.position.x < enemy.position.x ? enemy.position.x - player.position.x : player.position.x - enemy.position.x;
    yOffset = player.position.y < enemy.position.y ? enemy.position.y - player.position.y : player.position.y - enemy.position.y;
    distance = player.radius + enemy.radius;
    if(xOffset <= distance && yOffset <= distance)
    {
      enemy.collidedWith(player);
      player.collidedWith(enemy);
    }
  }
}

/* function resizeCanvas()
{
  canvas.width = document.body.clientWidth - (2 * bW);
  canvas.height = document.body.clientHeight - (2 * bW);
} */

function generateEnemies(level)
{
  /* fs.readFile(`./levels/level${level}`, {encoding: 'ascii', flag: 'r'}, (err, content) =>
  {
    if(err)
    {
      throw err;
    }

    let substrs = content.split('\n');
    let speed = parseInt(substrs[0]);
    substrs.splice(0, 1);
    for(let substr of substrs)
    {
      let coordinates = substr.split(/, /g);
      global.add(new Enemy({x: parseInt(coordinates[0]), y: parseInt(coordinates[1])}, speed), 'enemies');
    }
  }); */

  const enemyWidth = 60;
  const entityCount = parseInt(global.canvas.width) / enemyWidth;
  for(let i = 0; i < (level + 1) * entityCount / 10; i++)
  {
    for(let j = 0; j < entityCount; j++)
    {
      let enemy = new Enemy({x: 30 + j * enemyWidth, y: 30 + i * enemyWidth}, (level + 1) / 10);
      global.add(enemy, 'enemies');
    }
  }
}

//resizeCanvas();
global.player = new Player({x: canvas.width / 2, y: canvas.height - 20});

let generate = true;

function gameLoop()
{
  let handle = requestAnimationFrame(gameLoop);
  if(generate)
  {
    generateEnemies(0);
    generate = false;
  }
  if(player.lifes <= 0)
  {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.font = '30px TimesNewRoman';
    const gameOver = 'Game Over';
    let size = ctx.measureText(gameOver);
    ctx.fillText(gameOver, canvas.width / 2 - size.width, canvas.height / 2);
    cancelAnimationFrame(handle);
    return;
  }

  player.update();
  for(let projectile of global.projectiles)
  {
    projectile.update();
  }
  for(let enemy of global.enemies)
  {
    enemy.update();
  }
  checkCollision();
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  drawUI();
  player.draw(ctx);
  for(let projectile of global.projectiles)
  {
    projectile.draw(ctx);
  }
  for(let enemy of global.enemies)
  {
    enemy.draw(ctx);
  }
}

function drawUI()
{
  ctx.font = '30px TimesNewRoman';
  ctx.fillStyle = 'orange';
  const gameOver = `:Lifes: ${player.lifes}`;
  let size = ctx.measureText(gameOver);
  ctx.fillText(gameOver, 5, 25);
}

if(ctx)
{
  gameLoop();
}